[[_TOC_]]

# Gitlab Helmfiles

A monorepo for [Helm](https://helm.sh/) release configuration, managed by
[Helmfile](https://github.com/helmfile/helmfile).

## Concepts

### The tools

This section aims to describe at a high level what the tools we use are and how
we use them, without reproducing documentation that is better written (and more
up to date) in the repositories and websites of these tools themselves. It's
recommended to familiarize yourself with these tools as early as possible.

Helm is a templating system for Kubernetes resource manifests. A Helm chart is a
bundle of resource templates that can take parameters ("values"). A Helm chart
therefore describes a paramaterized deployment configuration that is
environment-agnostic. Helm charts are distributed from Helm repositories, and
Helm can act as a package manager to manage repositories and charts.

An instantiated deployment of a Helm chart is called a Helm release. We'll refer
to these as "releases" for short. We can say that a release is a deployment of a
Helm chart in an environment, where an environment is the combination of a set
of parameters ("values") and a Kubernetes target.

Helmfile is a management system for Helm releases. It allows the mapping of
charts to values to be declared in files, and for values to be declared at
various levels, in a way roughly analogous to Chef attribute precedence
(environment-level vs role-level etc).

### Repository structure

This repository is a monorepo for some Helmfile-managed Helm releases: all of
the deployments we use to manage gitlab.com except for the GitLab chart itself.

:exclamation: The `helm` chart deployment of the [Gitlab Helm
Chart](https://gitlab.com/gitlab-org/charts/gitlab) for Gitlab.com itself is
handled in the
[gitlab-com](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com)
repository instead. This is because of the very specific and very unique
challenges with deploying Gitlab.com into Kubernetes alongside our existing VM
based infrastructure. :exclamation:

The next subsections discuss the important files and directories at the top
level of this repository. It's recommended to familiarize yourself with Helm and
Helmfile first before continuing.

#### Releases

Each subdirectory of `releases` contains 1 (or sometimes more!) release. See
`releases/<release name>/helmfile.yaml` for a description about what charts are
deployed to which environments, and the differing config that gets passed into
each.

You'll see references to other files in `helmfile.yaml`, which contain values
for releases. Where values are coming from may be confusing at first - some of
these are in the release directory and are clearly either general values, or
environment-specific overrides, but some are in another part of the file tree
called "bases". Some of these files themselves appear to be templates, taking
extra values to produce values. This document will return to this directory to
tie these concepts together after explaining the other directories.

Some releases begin with a number. This is to enforce ordering on `helmfile
apply`, because these releases are dependencies of later releases. Non-numbered
releases must be deployed after the numbered ones, but in no particular order.

#### Bases

This directory contains 2 files: `helmDefaults.yaml`, which directs `helmfile`
on global configuration to pass to `helm`, and `environments.yaml`. This latter
file defines our various environments, and contains values which either need to
be shared between multiple releases (and so are DRYed out here), or are required
for "double-templating" nested values into values files in releases.

This last purpose should be used sparingly, and we'll return to it later.

#### Charts

Some local charts exist alongside releases to add extra resources required alongside
the main application, they are usually named in the format `<app>-extras`, and
are referenced from the `helmfile.yaml` file.
Any charts that we need to build ourselves to run a full application/service, should be
maintained at [gl-infra/charts](https://gitlab.com/gitlab-com/gl-infra/charts), where
they can be versioned and sourced per environment following best practices.

#### helmfile.yaml

A top-level helmfile.yaml, that declares a dependency on the `helmfile.yaml` in
each release directory. This is used to allow us to deploy and upgrade our
infrastructure in one command, as occurs in CI.

##### Patching helm releases with helmfile

In very extrordinary situations, we might have the need to patch the manifests
that come from an upstream helm chart to support configurations or options that
the upstream helm chart doesn't have. In these situations, we leverage
helmfiles [built in support for patching using kustomize](https://helmfile.readthedocs.io/en/latest/advanced-features/#adhoc-kustomization-of-helm-charts)
to do the patching. The definitions of these patches are defined within the
helmfile for the release itself, and the goal is to use this as a temporary
measure and try to push changes into the upstream chart to support the needed
functionality over time.

#### Tying it all together

Each release in this repository (which should be unrelated to each other, but
aren't necessarily - see "Gotchas" section) declares 1 or more Helmfile releases
in a `helmfile.yaml`. A release consists of a deployment of a chart (either
downloaded from a repository or referenced by relative path) to an environment,
taking values that may vary by environment. These values are found either in
`helmfile.yaml`, values files referenced from that helmfile, or are found in the
base environments config.

The set of environments is shared by all releases, but not all releases deploy
to every environment.

Helmfile values files can themselves be templates, and the values referenced
here originate from environments.yaml.

## Usage

### Required tooling

See the [common](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/common)
repo for tool versions used in CI. `.tool-versions` is asserted in CI to contain
versions equal to those in the CI image, and can be used by the
[asdf](https://github.com/asdf-vm/asdf) version manager.

If you're using `asdf`, install the relevant plugins and tools:

```
asdf plugin add helm
asdf plugin add helmfile

asdf install
```

Once you have the right versions of helm and helmfile, you can run
`../common/bin/install-helm-plugins.sh` to ensure that your plugin versions
match CI.

### Manual

⚠️ Some chart dependencies are fetched from the OCI registry on `ops.gitlab.net`, so you will need to be logged into it first (you will only need to do it once, or until your PAT expires). [Create a Personal Access Token for your user on `ops.gitlab.net`](https://ops.gitlab.net/-/profile/personal_access_tokens) with the `read_registry` scope and then run `helm registry login registry.ops.gitlab.net` with your username and PAT:
```
$ helm registry login registry.ops.gitlab.net
Username: jdoe
Password: <your PAT>
Login Succeeded
```

Invoking the `helmfile` CLI in a directory containing a `helmfile.yaml` will
allow a release to be deployed to an environment. For example:

```
$ cd releases/fluentd-elasticsearch
helmfile -e minikube <diff|apply>
```

Will either run `helm diff`, or actually install/upgrade the releases defined in
the helmfile.yaml, **to the Kubernetes that your kubectl context is currently
targetting**.

It is extremely important to ensure that your kubectl context is pointed at the
API server you intend to target.

Each helmfile environment maps 1:1 with a Kubernetes cluster. This mapping is
not encoded in the repo, and is only documented here.

TODO a table showing mappings of environment names to clusters, or possibly this
could be done by comments in environments.yaml?

:exclamation: Applying to the `pre` environment can be done freely as that is
considered a testing and development environment. For all other environments,
applying changes should **only** be done through CI Jobs :exclamation:

If you need to target the gstg or gprd clusters to query the API (e.g. because
you are debugging a problem), follow [this
guide](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/uncategorized/k8s-operations.md).

### Automated

We use GitLab CI pipelines that run on [a push-mirror of this
repository](https://ops.gitlab.net/gitlab-com/gl-infra/k8s-workloads/gitlab-helmfiles)
to run diffs on MRs, and to apply the configuration for each commit to master.
We use a mirror outside of gitlab.com so that we can deploy fixes in the
unfortunate event of a gitlab.com outage.

`.gitlab-ci.yml` (and the configured CI variables) should be seen as a source of
truth for what we automate in reaction to an event.

In general, we upgrade/install every Helm release in every environment that it
is configured to deploy to on every master pipeline.

### Bootstrapping new clusters

#### Creating a new environment

Every cluster must have a unique environment for Helm, there should be a new environment defined in https://ops.gitlab.net/gitlab-com/gl-infra/k8s-workloads/gitlab-helmfiles/-/blob/master/bases/environments.yaml that inherits the right values depending on whether it is staging or production.

After the environment is defined, CI jobs will need to be created in the [gitlab-ci.yml](https://ops.gitlab.net/gitlab-com/gl-infra/k8s-workloads/gitlab-helmfiles/-/blob/master/.gitlab-ci.yml) for gitlab-helmfiles.

See [Example MR for the production zonal clusters](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-helmfiles/-/merge_requests/239) for creating a new one.

#### Helmfile

:warning: **New clusters _that have Calico as their Network Policy agent_ must
run the following on the cluster, otherwise a deployment of the `calico-node-autoscaler` will fail.

```
kubectl -n kube-system annotate cm calico-node-vertical-autoscaler meta.helm.sh/release-name=calico-node-autoscaler
kubectl -n kube-system annotate cm calico-node-vertical-autoscaler meta.helm.sh/release-namespace=kube-system
kubectl -n kube-system label cm calico-node-vertical-autoscaler app.kubernetes.io/managed-by=Helm
```

_This may go away pending outcome of
https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/856_

##### Example Error

An example error that the above would fix looks similar to the following:

```
Error: rendered manifests contain a resource that already exists. Unable to continue with install: ConfigMap "calico-node-vertical-autoscaler" in namespace "kube-system" exists and cannot be imported into the current release:
  invalid ownership metadata; label validation error: missing key "app.kubernetes.io/managed-by": must be set to "Helm";
  annotation validation error: missing key "meta.helm.sh/release-name": must be set to "calico-node-autoscaler";
  annotation validation error: missing key "meta.helm.sh/release-namespace": must be set to "kube-system"
```

#### Applying the configuration

The `00-gitlab-ci-accounts` release installs RBAC resources required for CI
pipelines to manipulate cluster state. As such, it must be installed by an
already authorized role, i.e. an SRE from their laptop, before CI pipelines can
operate against a new environment.

Some charts install CRDs that are instantiated by other charts. Common examples
of this are the Prometheus, PrometheusRule, PodMonitor and ServiceMonitor
resources, that are declared by the prometheus-operator's helm chart. It's
common for software packaged by helm to declare its own monitoring (rules, jobs,
or even whole prometheus shards) next to the software itself.

This latter example is only a problem when such releases are declared in the
same helmfile. As helm evaluates templates for the releases that depend on CRDs
that don't exist yet, it throws an error.

Bootstrap new clusters by running the following command from your laptop:

```
helmfile -e ENV -l bootstrap=true apply
```

**Note**: Due to an ugly CRD dependency issue there is a workaround that needs to be done for monitoring, it's possible this will be resolved with Helm3, see https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-helmfiles/-/merge_requests/222#note_416781567 . If bootstrapping a new cluster is failing due to CRD dependency issues, try [this workaround](https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/1175#note_420650931).

Then, after configuring a new GitLab CI environment with secrets to access the
cluster in the same way existing environments are configured, those pipelines
should be able to run helm against that cluster.

#### Check Prometheus ingress and IAP

In the GCP console, you will need to ensure that the load balancers have healthy backends.
If backends are not healthy, we have seen issues with healthchecks not created properly.
To work around this, you may need to disable the ingress and then re-enable it.

To enable IAP, you will need to add `infrastructure@gitlab.com` as an `IAP-secured Web App User` on the [IAP](https://console.cloud.google.com/security/iap?project=gitlab-production) configuration page.
See other environments as an example

## Development

### minikube

There is a "minikube" environment defined in environments.yaml. This is
different from the other environments in that it's not a shared cluster, but is
intended to be used with a local minikube installation.

### Development Guidelines

These are some general guidelines for developing and reviewing changes to
releases and charts. As with everything, this is a living document and can be
MR'ed.

#### Releases

1. A release must contain a README that explains:
   1. What this release does, and why
   1. What environments this release targets
   1. Any external dependencies that must be provisioned outside of a Helm
     workflow (e.g. with Terraform), and injected as values. This includes
     schemas for secrets (see Secrets section below).
1. Releases must be deployable in the minikube environment.
   1. Not every resource or value makes sense in minikube. These should be
     conditionalized in the chart, or in the Helmfile values as appropriate.
1. Use the environments.yaml values sparingly.
   1. It's encouraged to use these for general values that are shared across
     several releases.
   1. It's less encouraged to use these for "double-templating", to perform
     templating logic in Helmfile values files. If this logic can be pushed into
     the chart itself, that's often preferable.
1. Avoid convoluted templating logic: try to use a single general values file,
   overridden as needed with named files for each environment.
   1. Of course this won't always be feasible.

#### Charts

1. Charts must be environment-agnostic.
1. Values must be clearly documented in the `values.yaml` file. Mandatory values
   should be shown as commented-out keys.
1. Templating YAML is a painful and thankless task. DRY code is nice, but not at
   the cost of introducing too much complex logic to existing charts. There is a
   subjective point at which starting a new chart for a release makes sense.
   Discuss this with other contributors before getting started, if in doubt.
1. Do not use `metadata.namespace` in helm chart templates. Helm ignores the namespace config once the release is installed so you might end up with releases unintentionally spanning multiple namespaces. For more details see: https://github.com/helm/helm/issues/5465

#### Secrets

See the runbook [External Secrets in Kubernetes](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/vault/usage.md#external-secrets-in-kubernetes).

#### Testing RBAC Policies

Testing RBAC permissions is easier when performing the work on live clusters.
If this is not possible for any reason, utilizing this guide:
https://medium.com/@HoussemDellai/rbac-with-kubernetes-in-minikube-4deed658ea7b
proved helpful in order to test permissions using minikube.

## Gotchas and Rough Edges

No procedure is perfect, and we're no different.

### Implicit inter-release dependencies

Helmfile releases should be independent of each other, with inter-chart
dependencies resolved using Helm's own support for that intra-release, but
currently that isn't quite the case. There are several implicit inter-release
dependencies that prevent a fresh `helmfile -e minikube apply` on the top-level
helmfile (which deploys releases in alphabetical order) from working:

* Several releases depend on prometheus-operator CRDs that are installed by
  gitlab-monitoring
* For the "real" GKE environments, some releases depend on gitlab-storage
* Our automated deployments depend on gitlab-ci-accounts, which must be manually
  installed out-of-band by someone who already has permission (this needs to be
  verified).

This list may be out of date, and ideally these implicit dependencies will be
removed.

### Helm woes

#### Helm is not convergent

Helm is not a convergent system, unlike `kubectl diff`, Chef, or Ansible. `helm
diff` simply compares the freshly-generated manifests with those stored for the
latest deployed release version in Helm's state secret. If you manually make a
change to a Helm-managed resource, `helm diff` will not show it, and therefore
`helm upgrade` won't converge the new resource configuration unless you make
another change to force that process, such as adding an annotation:

```yaml
metadata:
  annotations:
    accidentally-made-manual-change-on: 2020-06-23
```

#### Helm rollback

Ideally, botched deployments are fixed by rolling forward, and `helmfile
apply`-ing a fix. This isn't always possible, and sometimes it's necessary to
use Helm's rollback functionality. This has to be done [from a
console](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/uncategorized/k8s-operations.md),
and we don't currently automatically install helm on console shells.

In general, the following commands are useful.

```
helm list
helm history <release name>
helm rollback <release name> <version number>
```

You must roll back to a release in a non-errored state, and then you should be
able to roll forward to a non-broken config.

#### Helm release updates ignore changes to namespaces

The kubernetes namespace to which a release is deployed is only considered during initial installation. Once a release is deployed, it is impossible to move the release to another namespace using helm.

To work around this limitation you'll need to remove the existing release completely and run `helmfile apply` with the config containing the new namespace (so that the `helm` command tries to install a fresh release). It might prove difficult to perform this operation without purging the old release, for example in a situation requiring no downtime. You would need to create the objects without specifying the namespace in the metadata: https://github.com/helm/helm/issues/5465 and you'll also need to perform surgery on the state of helm releases (by default stored in secrets in `kube-system` namespace).

#### Common mistakes

1. `default` treats all falsy values as nil: https://github.com/helm/helm/issues/3308
