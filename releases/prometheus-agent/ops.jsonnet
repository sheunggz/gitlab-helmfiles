{
  local c = (import './config.libsonnet'),
  local googleCloudProject = 'gitlab-ops',

  // Enabled scrape Jobs
  JobEnabled: c.scrapeJobs {
    NodeExporterGCE: true,
    Fluentd: true,
    Mtail: true,
    Pushgateway: true,
    Stackdriver: true,
    Runner: false,
    Wmi: false,
  },

  customConfigs: {
    // Add scrapeConfigs with their relevant projects, zones and port
    gceSDConfigs:
      c.gceSDConfig('NodeExporterGce', googleCloudProject, c.zones.all, 9100) +
      c.gceSDConfig('Fluentd', googleCloudProject, c.zones.all, 24231) +
      c.gceSDConfig('Mtail', googleCloudProject, c.zones.all, 3903) +
      c.gceSDConfig('Pushgateway', googleCloudProject, ['us-east1-c'], 9091) +
      c.gceSDConfig('Stackdriver', googleCloudProject, c.zones.default, 9255),
  },
}
