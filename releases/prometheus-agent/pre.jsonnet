{
  local c = (import './config.libsonnet'),
  local googleCloudProject = 'gitlab-pre',

  // Enabled scrape Jobs
  JobEnabled: c.scrapeJobs {
    NodeExporterGCE: true,
    Fluentd: true,
    Mtail: true,
    Haproxy: true,
    Gitaly: true,
    Redis: true,
    Praefect: true,
    Stackdriver: true,
    Runner: false,
    Wmi: false,
  },

  customConfigs: {
    // Add scrapeConfigs with their relevant projects, zones and port
    gceSDConfigs:
      c.gceSDConfig('NodeExporterGce', googleCloudProject, c.zones.all, 9100) +
      c.gceSDConfig('Fluentd', googleCloudProject, c.zones.all, 24231) +
      c.gceSDConfig('Mtail', googleCloudProject, c.zones.all, 3903) +
      c.gceSDConfig('Haproxy', googleCloudProject, c.zones.default, 9101) +
      c.gceSDConfig('Gitaly', googleCloudProject, c.zones.default, 9236) +
      c.gceSDConfig('Gitaly', googleCloudProject, c.zones.default, 9236) +
      c.gceSDConfig('Redis', googleCloudProject, c.zones.default, 9121) +
      c.gceSDConfig('Praefect', googleCloudProject, c.zones.default, 9652) +
      c.gceSDConfig('Stackdriver', googleCloudProject, c.zones.central, 9255),
  },
}
