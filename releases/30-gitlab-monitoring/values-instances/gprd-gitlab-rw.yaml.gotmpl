---
fullnameOverride: "gitlab-rw"
prometheus:
  agentMode: true
  thanosService:
    enabled: false
  podDisruptionBudget:
    enabled: true
  service:
    clusterIP: ""
    type: ClusterIP
    additionalPorts: []
  prometheusSpec:
    # release notes https://github.com/prometheus/prometheus/releases
    # renovate: datasource=docker depName=quay.io/prometheus/prometheus versioning=docker
    version: v2.51.0
    # See: https://gitlab.com/gitlab-com/gl-infra/reliability/-/issues/24728
    image:
      # renovate: datasource=docker depName=quay.io/prometheus/prometheus versioning=docker
      tag: v2.51.0
    enableFeatures:
      # https://github.com/prometheus/prometheus/pull/10498
      - auto-gomaxprocs
    additionalArgs: ""
    replicas: 2
    replicaExternalLabelName: __replica__
    remoteWrite:
      - url: https://mimir.ops.gke.gitlab.net/api/v1/push
        name: mimir
        queueConfig:
          # capacity should ideally be 3-10x that of max sample size, x5 as a good default.
          capacity: 10000
          # shards effect concurrency, this should often be reduced if maxSamplesPerSend is increased.
          maxShards: 200
          # max samples sends more samples per request increasing packet size.
          maxSamplesPerSend: 2000
        headers:
          X-Scope-OrgID: gitlab-gprd
        basicAuth:
          username:
            name: prometheus-remote-write-auth
            key: username
          password:
            name: prometheus-remote-write-auth
            key: password
    storageSpec:
      volumeClaimTemplate:
        spec:
          storageClassName: pd-ssd
          resources:
            requests:
              storage: 200Gi
    resources:
      requests:
        cpu: 30
        memory: 300Gi
      limits:
        memory: 300Gi

    serviceMonitorSelectorNilUsesHelmValues: false
    serviceMonitorSelector: ""

    podMonitorSelectorNilUsesHelmValues: false
    podMonitorSelector: ""

    ruleSelectorNilUsesHelmValues: true
    ruleNamespaceSelector: ""
    ruleSelector: ""

  serviceAccount:
    annotations:
      iam.gke.io/gcp-service-account: prometheus-rw-{{ .Environment.Values | get "env_prefix" .Environment.Name }}@{{ .Environment.Values.google_project }}.iam.gserviceaccount.com
