---
prometheus:
  prometheusSpec:
    # release notes https://github.com/prometheus/prometheus/releases
    # renovate: datasource=docker depName=quay.io/prometheus/prometheus versioning=docker
    version: v2.51.0
    # See: https://gitlab.com/gitlab-com/gl-infra/reliability/-/issues/24728
    image:
      # renovate: datasource=docker depName=quay.io/prometheus/prometheus versioning=docker
      tag: v2.51.0
    enableFeatures:
      # https://github.com/prometheus/prometheus/pull/10498
      - auto-gomaxprocs
    replicas: 2
    replicaExternalLabelName: __replica__
    remoteWrite:
      - url: https://mimir.ops.gke.gitlab.net/api/v1/push
        name: mimir
        queueConfig:
          capacity: 10000
          maxShards: 200
          maxSamplesPerSend: 2000
        headers:
          X-Scope-OrgID: gitlab-gprd
        basicAuth:
          username:
            name: prometheus-remote-write-auth
            key: username
          password:
            name: prometheus-remote-write-auth
            key: password
    storageSpec:
      volumeClaimTemplate:
        spec:
          storageClassName: pd-ssd
          resources:
            requests:
              storage: 200Gi
    resources:
      requests:
        cpu: 30
        memory: 250Gi
      limits:
        memory: 250Gi
