---
fullnameOverride: "gitlab-rw"
prometheus:
  agentMode: true
  thanosService:
    enabled: false
  podDisruptionBudget:
    enabled: true
  service:
    clusterIP: ""
    type: ClusterIP
    additionalPorts: []
  prometheusSpec:
    # release notes https://github.com/prometheus/prometheus/releases
    # renovate: datasource=docker depName=quay.io/prometheus/prometheus versioning=docker
    version: v2.51.0
    # See: https://gitlab.com/gitlab-com/gl-infra/reliability/-/issues/24728
    image:
      # renovate: datasource=docker depName=quay.io/prometheus/prometheus versioning=docker
      tag: v2.51.0
    enableFeatures:
      # https://github.com/prometheus/prometheus/pull/10498
      - auto-gomaxprocs
    replicas: 2
    replicaExternalLabelName: __replica__
    remoteWrite:
      - url: https://mimir.ops.gke.gitlab.net/api/v1/push
        name: mimir
        headers:
          X-Scope-OrgID: gitlab-ops
        basicAuth:
          username:
            name: prometheus-remote-write-auth
            key: username
          password:
            name: prometheus-remote-write-auth
            key: password
    storageSpec:
      volumeClaimTemplate:
        spec:
          storageClassName: pd-ssd
          resources:
            requests:
              storage: 100Gi
    resources:
      requests:
        cpu: "4"
        memory: 20Gi
      limits:
        memory: 20Gi

    serviceMonitorSelectorNilUsesHelmValues: false
    serviceMonitorSelector: ""

    podMonitorSelectorNilUsesHelmValues: false
    podMonitorSelector: ""

    ruleSelectorNilUsesHelmValues: true
    ruleNamespaceSelector: ""
    ruleSelector: ""

    secrets: null
    additionalScrapeConfigs: null

  serviceAccount:
    annotations:
      iam.gke.io/gcp-service-account: prometheus-rw-{{ .Environment.Values | get "env_prefix" .Environment.Name }}@{{ .Environment.Values.google_project }}.iam.gserviceaccount.com
