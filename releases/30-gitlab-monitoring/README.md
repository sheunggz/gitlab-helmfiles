# gitlab-monitoring

Deploys the Prometheus stack.

This helmfile deploys a few releases. The core is the [prometheus-operator helm
chart](https://github.com/helm/charts/tree/master/stable/prometheus-operator).
Part of this chart's configuration is the names of secret objects that the
operator user is supposed to create out of band. We create these objects in the
"monitoring-secrets" and "gitlab-monitoring-secrets" releases, which deploy local charts.

## Usage

Manually create a few resources that are referenced by the secret charts:

1. Follow [Creating OAuth credentials](https://cloud.google.com/iap/docs/enabling-kubernetes-howto#oauth-credentials). You can use [gstg](https://console.cloud.google.com/apis/credentials/oauthclient/65580314219-jqstgo44nhpp9v525u5ktlqbbum791jk.apps.googleusercontent.com?project=gitlab-staging-1) as an example. When filling in the form add the following including what GCP documentation tells you.
    - Add `Authorized JavaScript origins` with: `https://prometheus-gke.<env>.gitlab.net`
    - Add `Authorized redirect URIs` with: `https://prometheus-gke.<env>.gitlab.net/_gcp_gatekeeper/authenticate`
1. Save the IAP Oauth credentials in Vault under `k8s/env/<env>/ns/monitoring/iap-oauth`:
   ```sh
   vault kv put k8s/env/gstg/ns/monitoring/iap-oauth client_id=... client_secret=...
   ```
1. thanos-sidecar will need a service account private key that grants access to
   the environment's prometheus bucket.
    1. If the [storage-buckets](https://ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/storage-buckets)
       terraform module is installed for your environment, the bucket and
       service account will already exist.
    1. Manually create a private key for the `<env>-prometheus-sa` service
       account. We don't create these in Terraform to avoid storing the private
       key in unencrypted terraform state.
    1. Save the service account key in Vault under `k8s/env/<env>/ns/monitoring/prometheus-sa` with the key `key`:
       ```sh
       vault kv put k8s/env/gstg/ns/monitoring/prometheus-sa key=@service_account_key.json
       ```
    1. In the future this flow can be modified to use workload identity.

This release supports Google-managed certificates, so you don't need to
configure TLS certificates if you are deploying onto a GKE cluster that supports
them - which you almost definitely are, if you're reading this in order to
create a new deployment.

Create a file `ENV.yaml.gotmpl` for your new environment, containing any
environment-specific overrides you might need. See existing environments to get
a sense of what you might need.

In `bases/environments.yaml`, create some configuration for your environment:

```yaml
environments:
  an-env:
    values:
      - gitlab_monitoring:

          # Most environments won't need an Alertmanager
          alertmanager:
            installed: false
            domain: "set-if-installed-is-true.gitlab.net"
            # A feature flag that creates an extra headless service for alertmanager.
            extras_installed: false

          google_managed_cert:
            enabled: true

          # A feature flag for GKE versions that use non-beta BackendConfigs.
          # Will be removed when all clusters are upgraded to 1.16
          backendconfig_api_version: "cloud.google.com/v1"
```

You should now be able to deploy this environment:

`helmfile -e ENV -l group=monitoring diff`

You should configure it in this repo's CI, as we do with all environments.

### Bootstrapping new clusters
> Please note that you need to label your `minikube` node with `default`, in order for prometheus' pods to run on it.
> Just run `kubectl label nodes YOUR_MINIKUBE_NODE_NAME type=default`

See the "Bootstrapping" new clusters in the top-level README.md. On fresh
clusters, e.g. a new minikube/dev environment, the prometheus-operator CRDs must
be installed before charts that depend on them.

Run:

```
helmfile -e ENV -l bootstrap=true apply
```

Which installs the prometheus-operator (and its CRDs). Now you will be able to
run helmfile diffs and applies without a label filter.

If you run into errors like this: `skipping unknown hook: "crd-install"`, the
CRDs will need to be manually installed. Utilize the following instructions to
do so:

1. Check the version of the prometheus operator chart being utilized, (in the
   helmfile.yaml)
1. Check the README of the operator project for any new CRD's that may not be
   listed below
1. Copy and paste the below lines into a shell with the correct Kubenetes
   context, modifying the version being utilized if necessary

```
kubectl apply -f https://raw.githubusercontent.com/prometheus-operator/prometheus-operator/0a38647379a5e93f639bf8e634deabcc32e01fb6/example/prometheus-operator-crd/monitoring.coreos.com_alertmanagerconfigs.yaml
kubectl apply -f https://raw.githubusercontent.com/prometheus-operator/prometheus-operator/0a38647379a5e93f639bf8e634deabcc32e01fb6/example/prometheus-operator-crd/monitoring.coreos.com_alertmanagers.yaml
kubectl apply -f https://raw.githubusercontent.com/prometheus-operator/prometheus-operator/0a38647379a5e93f639bf8e634deabcc32e01fb6/example/prometheus-operator-crd/monitoring.coreos.com_podmonitors.yaml
kubectl apply -f https://raw.githubusercontent.com/prometheus-operator/prometheus-operator/0a38647379a5e93f639bf8e634deabcc32e01fb6/example/prometheus-operator-crd/monitoring.coreos.com_probes.yaml
kubectl apply -f https://raw.githubusercontent.com/prometheus-operator/prometheus-operator/0a38647379a5e93f639bf8e634deabcc32e01fb6/example/prometheus-operator-crd/monitoring.coreos.com_prometheuses.yaml
kubectl apply -f https://raw.githubusercontent.com/prometheus-operator/prometheus-operator/0a38647379a5e93f639bf8e634deabcc32e01fb6/example/prometheus-operator-crd/monitoring.coreos.com_prometheusrules.yaml
kubectl apply -f https://raw.githubusercontent.com/prometheus-operator/prometheus-operator/0a38647379a5e93f639bf8e634deabcc32e01fb6/example/prometheus-operator-crd/monitoring.coreos.com_servicemonitors.yaml
kubectl apply -f https://raw.githubusercontent.com/prometheus-operator/prometheus-operator/0a38647379a5e93f639bf8e634deabcc32e01fb6/example/prometheus-operator-crd/monitoring.coreos.com_thanosrulers.yaml
```
