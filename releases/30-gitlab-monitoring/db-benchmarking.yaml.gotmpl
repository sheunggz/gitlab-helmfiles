---
prometheus:
  prometheusSpec:
    # release notes https://github.com/prometheus/prometheus/releases
    # renovate: datasource=docker depName=quay.io/prometheus/prometheus versioning=docker
    version: v2.47.2
    # See: https://gitlab.com/gitlab-com/gl-infra/reliability/-/issues/24728
    image:
      # renovate: datasource=docker depName=quay.io/prometheus/prometheus versioning=docker
      tag: v2.47.2
    storageSpec:
      volumeClaimTemplate:
        spec:
          storageClassName: ssd
          resources:
            requests:
              storage: 50Gi
    resources:
      requests:
        cpu: "2"
        memory: 10Gi
    thanos:
      # renovate: datasource=docker depName=quay.io/thanos/thanos versioning=docker
      version: v0.32.5
      # ReadyTimeout is the maximun time that the Thanos sidecar will wait for
      # Prometheus to be ready (default 10m). Change to 90m (2023-11-17) to
      # ensure that we can process the WAL when Prometheus is SIGKILL
      readyTimeout: 90m
      # Object Storage Configuration
      objectStorageConfig:
        # required for kube-prometheus-stack v52
        existingSecret:
          key: objstore.yaml
          name: prometheus-thanos-objstore-v3
  ingress:
    hosts:
      - "prometheus-gke.{{ .Environment.Name }}.gitlab.net"
    paths:
      - /*
  service:
    loadBalancerIP: "10.255.42.2"

prometheusOperator:
  logLevel: all
