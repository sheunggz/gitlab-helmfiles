---
podMonitors:
  # Sidekiq
  sidekiq:
    enabled: false
    name: sidekiq
    additionalLabels:
      gitlab.com/prometheus-instance: prometheus-system
    namespaceSelector:
      any: true
    selector:
      matchLabels:
        app: sidekiq
    podMetricsEndpoints:
      - port: http-metrics
        path: /metrics
        metricRelabelings:
          - sourceLabels: ["__name__"]
            regex: "http_request_duration_seconds_.*"
            action: drop
          # In
          # https://gitlab.com/gitlab-com/gl-infra/reliability/-/issues/15955
          # we've created a tmp shard for AuthorizedProjectsWorker, so we need
          # to mark it as throttled for the SLO to be correct.
          - sourceLabels: [worker, urgency]
            regex: AuthorizedProjectsWorker;high
            targetLabel: urgency
            replacement: throttled
          - sourceLabels: [__name__,storage,storage_shard]
            regex: gitlab_redis_client_.*;queues;(.*)
            targetLabel: shard
            replacement: $1
            action: replace
        relabelings:
          - sourceLabels: ["__meta_kubernetes_pod_ready"]
            regex: "^false$"
            action: drop
          - action: labelmap
            regex: "__meta_kubernetes_pod_label_(.+)"
          - sourceLabels:
              - __meta_kubernetes_pod_node_name
            targetLabel: node
  zoekt-webserver:
    enabled: false
    name: zoekt-webserver
    additionalLabels:
      gitlab.com/prometheus-instance: prometheus-system
    namespaceSelector:
      any: true
    selector:
      matchLabels:
        app.kubernetes.io/name: gitlab-zoekt
    podMetricsEndpoints:
      - port: webserver
        path: /metrics
        relabelings:
          - action: labelmap
            regex: "__meta_kubernetes_pod_label_(.+)"
          - sourceLabels:
              - __meta_kubernetes_pod_node_name
            targetLabel: node
  zoekt-indexer:
    enabled: false
    name: zoekt-indexer
    additionalLabels:
      gitlab.com/prometheus-instance: prometheus-system
    namespaceSelector:
      any: true
    selector:
      matchLabels:
        app.kubernetes.io/name: gitlab-zoekt
    podMetricsEndpoints:
      - port: indexer
        path: /indexer/metrics
        relabelings:
          - action: labelmap
            regex: "__meta_kubernetes_pod_label_(.+)"
          - sourceLabels:
              - __meta_kubernetes_pod_node_name
            targetLabel: node

serviceMonitors:
  # Rails
  gitlab-rails:
    enabled: false
    name: gitlab-rails
    additionalLabels:
      gitlab.com/prometheus-instance: prometheus-system
    namespaceSelector:
      any: true
    # We are setting a label so that we can modify the job field so it
    # matches what we currently set in VM infra
    jobLabel: railsPromJob
    selector:
      matchLabels:
        app: webservice
    endpoints:
      - port: http-metrics-ws
        path: /metrics
        metricRelabelings:
          # In
          # https://gitlab.com/gitlab-com/gl-infra/reliability/-/issues/15955
          # we've created a tmp shard for AuthorizedProjectsWorker, so we need
          # to mark it as throttled for the SLO to be correct.
          - sourceLabels: [worker, urgency]
            regex: AuthorizedProjectsWorker;high
            targetLabel: urgency
            replacement: throttled
          - sourceLabels: [__name__,storage,storage_shard]
            regex: gitlab_redis_client_.*;queues;(.*)
            targetLabel: shard
            replacement: $1
            action: replace
        relabelings:
          - action: labelmap
            regex: "__meta_kubernetes_pod_label_(.+)"
          - sourceLabels:
              - __meta_kubernetes_pod_node_name
            targetLabel: node
  # Workhorse
  gitlab-workhorse:
    enabled: false
    name: gitlab-workhorse
    additionalLabels:
      gitlab.com/prometheus-instance: prometheus-system
    # We are setting a label so that we can modify the job field so it
    # matches what we currently set in VM infra
    jobLabel: workhorsePromJob
    namespaceSelector:
      any: true
    selector:
      matchLabels:
        app: webservice
    endpoints:
      - port: http-metrics-wh
        path: /metrics
        relabelings:
          - action: labelmap
            regex: "__meta_kubernetes_pod_label_(.+)"
          - sourceLabels:
              - __meta_kubernetes_pod_node_name
            targetLabel: node
  # Nginx
  nginx-ingress:
    enabled: false
    name: nginx-ingress
    additionalLabels:
      gitlab.com/prometheus-instance: prometheus-system
    namespaceSelector:
      any: true
    selector:
      matchLabels:
        app: nginx-ingress
    endpoints:
      - port: metrics
        path: /metrics
        interval: 10s
        relabelings:
          - action: labelmap
            regex: "__meta_kubernetes_pod_label_(.+)"
          - sourceLabels:
              - __meta_kubernetes_pod_node_name
            targetLabel: node
  # Registry
  registry:
    enabled: false
    name: registry
    additionalLabels:
      gitlab.com/prometheus-instance: prometheus-system
    namespaceSelector:
      any: true
    selector:
      matchLabels:
        app: registry
    endpoints:
      - port: http-metrics
        path: /metrics
        interval: 10s
        relabelings:
          - action: labelmap
            regex: "__meta_kubernetes_pod_label_(.+)"
          - sourceLabels:
              - __meta_kubernetes_pod_node_name
            targetLabel: node
  # KAS
  kas:
    enabled: false
    name: kas
    additionalLabels:
      gitlab.com/prometheus-instance: prometheus-system
    namespaceSelector:
      any: true
    selector:
      matchLabels:
        app: kas
    endpoints:
      - targetPort: 8151
        path: /metrics
        relabelings:
          - action: labelmap
            regex: "__meta_kubernetes_pod_label_(.+)"
  # Pages
  gitlab-pages:
    enabled: false
    name: gitlab-pages
    additionalLabels:
      gitlab.com/prometheus-instance: prometheus-system
    namespaceSelector:
      any: true
    # We are setting a label so that we can modify the job field so it
    # matches what we currently set in VM infra
    jobLabel: pagesPromJob
    selector:
      matchLabels:
        app: gitlab-pages
    endpoints:
      - targetPort: 9235
        path: /metrics
        relabelings:
          - action: labelmap
            regex: "__meta_kubernetes_pod_label_(.+)"
          - sourceLabels:
              - __meta_kubernetes_pod_node_name
            targetLabel: node
  # Shell
  gitlab-shell:
    enabled: false
    name: gitlab-shell
    additionalLabels:
      gitlab.com/prometheus-instance: prometheus-system
    namespaceSelector:
      any: true
    # We are setting a label so that we can modify the job field so it
    # matches what we currently set in VM infra
    jobLabel: shellPromJob
    selector:
      matchLabels:
        app: gitlab-shell
    endpoints:
      - port: http-metrics
        path: /metrics
        relabelings:
          - action: labelmap
            regex: "__meta_kubernetes_pod_label_(.+)"
          - sourceLabels:
              - __meta_kubernetes_pod_node_name
            targetLabel: node
  # Calico
  calico-node:
    enabled: true
    name: calico-node
    additionalLabels:
      gitlab.com/prometheus-instance: prometheus-system
    namespaceSelector:
      matchNames:
        - kube-system
    selector:
      matchLabels:
        gitlab-app: calico-node
    endpoints:
      - port: metrics
        path: /metrics
        relabelings:
          - action: labelmap
            regex: "__meta_kubernetes_pod_label_(.+)"
          - sourceLabels:
              - __meta_kubernetes_pod_node_name
            targetLabel: node
  # Thanos
  thanos-sidecar:
    enabled: true
    name: thanos-sidecar
    additionalLabels:
      gitlab.com/prometheus-instance: prometheus-system
    namespaceSelector:
      any: true
    selector:
      matchLabels:
        app: prometheus-operator-prometheus
    endpoints:
      - port: thanos-sidecar-metrics
        path: /metrics
        interval: 10s

  # Blackbox
  blackbox:
    enabled: false
    name: blackbox
    additionalLabels:
      gitlab.com/prometheus-instance: prometheus-system
    namespaceSelector:
      matchNames:
        - monitoring
    selector:
      matchLabels:
        app.kubernetes.io/name: prometheus-blackbox-exporter
        app.kubernetes.io/instance: prometheus-blackbox-exporter
    endpoints:
      - port: http
        path: /metrics
        relabelings:
          - action: labelmap
            regex: "__meta_kubernetes_pod_label_(.+)"
          - sourceLabels:
              - __meta_kubernetes_pod_node_name
            targetLabel: node
