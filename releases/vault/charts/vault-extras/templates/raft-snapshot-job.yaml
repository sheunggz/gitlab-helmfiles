---
apiVersion: batch/v1
kind: CronJob
metadata:
  name: {{ include "vault-extras.vaultRaftSnapshotJobName" . }}
  labels: {{- include "common.labels.standard" . | nindent 4 }}
    {{- if .Values.commonLabels }}
      {{- include "common.tplvalues.render" (dict "value" .Values.commonLabels "context" $) | nindent 4 }}
    {{- end }}
    {{- if .Values.raftSnapshot.labels }}
      {{- include "common.tplvalues.render" (dict "value" .Values.raftSnapshot.labels "context" $) | nindent 4 }}
    {{- end }}
  {{- if or .Values.commonAnnotations .Values.raftSnapshot.annotations }}
  annotations:
    {{- if .Values.commonAnnotations }}
      {{- include "common.tplvalues.render" ( dict "value" .Values.commonAnnotations "context" $ ) | nindent 4 }}
    {{- end }}
    {{- if .Values.raftSnapshot.annotations }}
      {{- include "common.tplvalues.render" (dict "value" .Values.raftSnapshot.annotations "context" $) | nindent 4 }}
    {{- end }}
  {{- end }}
spec:
  schedule: {{ .Values.raftSnapshot.schedule }}
  jobTemplate:
    spec:
      ttlSecondsAfterFinished: 3600
      template:
        metadata:
          labels:
            {{- include "common.labels.matchLabels" . | nindent 12 }}
            app.kubernetes.io/component: {{ include "vault-extras.vaultRaftSnapshotJobName" . }}
        spec:
          initContainers:
          - name: fetch-vault-raft-snapshot
            image: {{ .Values.raftSnapshot.images.vault.repository }}:{{ .Values.raftSnapshot.images.vault.tag | default "latest" }}
            imagePullPolicy: IfNotPresent
            command:
            - /bin/sh
            - -ec
            args:
            - |
              cd /vault/workdir

              echo "Logging into Vault..."
              vault login -no-print -method=gcp role=raft-snapshots service_account="${GOOGLE_SERVICE_ACCOUNT}"

              echo "Fetching Raft snapshot..."
              vault operator raft snapshot save raft.snap
            env:
              - name: GOOGLE_SERVICE_ACCOUNT
                value: {{ .Values.raftSnapshot.gcpServiceAccount }}
              - name: VAULT_ADDR
                value: {{ .Values.raftSnapshot.vaultAddr }}
              - name: VAULT_TLS_SERVER_NAME
                value: {{ .Values.raftSnapshot.tlsServerName }}
            volumeMounts:
              - mountPath: /vault/workdir
                name: workdir
          containers:
          - name: upload-vault-raft-snapshot
            image: {{ .Values.raftSnapshot.images.googleCloudSDK.repository }}:{{ .Values.raftSnapshot.images.googleCloudSDK.tag | default "latest" }}
            imagePullPolicy: IfNotPresent
            command:
            - /bin/sh
            - -ec
            args:
            - |
              SNAPSHOT_PATH="$(date +%Y/%m/%d)"
              SNAPSHOT_NAME="raft-$(date +%Y%m%d-%H%M%S%z).snap"

              echo "Uploading snapshot ${SNAPSHOT_NAME} to GCS bucket ${GCS_BUCKET}..."
              gsutil cp /vault/workdir/raft.snap "gs://${GCS_BUCKET}/${SNAPSHOT_PATH}/${SNAPSHOT_NAME}"
              rm -f /vault/workdir/raft.snap

              echo "Done!"
            env:
              - name: GCS_BUCKET
                value: {{ .Values.raftSnapshot.gcsBucket }}
            volumeMounts:
              - mountPath: /vault/workdir
                name: workdir
          {{- with .Values.raftSnapshot.nodeSelector }}
          nodeSelector:
            {{- toYaml . | nindent 12 }}
          {{- end }}
          {{- with .Values.raftSnapshot.tolerations }}
          tolerations:
            {{- toYaml . | nindent 12 }}
          {{- end }}
          restartPolicy: OnFailure
          serviceAccountName: {{ .Values.raftSnapshot.serviceAccountName }}
          volumes:
            - name: workdir
              emptyDir: {}
