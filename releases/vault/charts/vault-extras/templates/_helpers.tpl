{{/*
vault-raft-snapshot job name
*/}}
{{- define "vault-extras.vaultRaftSnapshotJobName" -}}
{{ include "common.names.fullname" . }}-raft-snapshot
{{- end }}

{{/*
vault-identity-entities-cleaner job name
*/}}
{{- define "vault-extras.vaultIdentityEntitiesCleanerJobName" -}}
{{ include "common.names.fullname" . }}-identity-entities-cleaner
{{- end }}

{{/*
vault-identity-entities-cleaner service account name
*/}}
{{- define "vault-extras.vaultIdentityEntitiesCleanerServiceAccountName" -}}
{{ include "common.names.fullname" . }}-identity-entities-cleaner
{{- end }}

{{/*
TLS secret name
*/}}
{{- define "vault-extras.tlsSecretName" -}}
{{ include "common.names.fullname" . }}-tls
{{- end }}
