# runway-exporter release

Google [Stackdriver Prometheus exporter](https://github.com/prometheus-community/stackdriver_exporter) used to scrape Stackdriver metrics API for [Runway](https://about.gitlab.com/handbook/engineering/infrastructure/platforms/tools/runway/).
