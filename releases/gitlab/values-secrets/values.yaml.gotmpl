---
clusterName: {{ .Environment.Values.cluster }}

externalSecrets:
  gitaly-secret-v1:
    refreshInterval: 0
    secretStoreName: gitlab-secrets
    target:
      creationPolicy: Owner
      deletionPolicy: Delete
    data:
      - remoteRef:
          key: "env/ops/ns/{{ .Release.Namespace }}/gitaly/secret"
          property: token
          version: "1"
        secretKey: token

  google-oauth2-v1:
    refreshInterval: 0
    secretStoreName: gitlab-secrets
    target:
      creationPolicy: Owner
      deletionPolicy: Delete
    data:
      - remoteRef:
          key: "env/ops/ns/{{ .Release.Namespace }}/auth/google-oauth2"
          property: provider
          version: "1"
        secretKey: provider

  kas-secret-v1:
    refreshInterval: 0
    secretStoreName: gitlab-secrets
    target:
      creationPolicy: Owner
      deletionPolicy: Delete
    data:
      - remoteRef:
          key: "env/ops/ns/{{ .Release.Namespace }}/kas/api"
          property: api-secret
          version: "1"
        secretKey: api-secret
      - remoteRef:
          key: "env/ops/ns/{{ .Release.Namespace }}/kas/api"
          property: private-api-secret
          version: "1"
        secretKey: private-api-secret

  postgres-credentials-v1:
    refreshInterval: 0
    secretStoreName: gitlab-secrets
    target:
      creationPolicy: Owner
      deletionPolicy: Delete
    data:
      - remoteRef:
          key: "env/ops/ns/{{ .Release.Namespace }}/postgres/credentials"
          property: password
          version: "1"
        secretKey: password

  rails-secrets-v1:
    refreshInterval: 0
    secretStoreName: gitlab-secrets
    target:
      creationPolicy: Owner
      deletionPolicy: Delete
    data:
      - remoteRef:
          key: "env/ops/ns/{{ .Release.Namespace }}/rails/secrets"
          property: secrets.yml
          version: "1"
        secretKey: secrets.yml

  rails-storage-v2:
    refreshInterval: 0
    secretStoreName: gitlab-secrets
    target:
      creationPolicy: Owner
      deletionPolicy: Delete
      template:
        data:
          connection: |
            provider: Google
            google_project: {{ .Environment.Values.google_project }}
            google_json_key_string: |
              {{ `{{ "{{-" }}` }} .service_account_key | nindent 2 }}
    data:
      - remoteRef:
          key: "env/ops/ns/{{ .Release.Namespace }}/google"
          property: service_account_key
          version: "1"
        secretKey: service_account_key

  redis-secret-v1:
    refreshInterval: 0
    secretStoreName: gitlab-secrets
    target:
      creationPolicy: Owner
      deletionPolicy: Delete
    data:
      - remoteRef:
          key: "env/ops/ns/{{ .Release.Namespace }}/redis/secret"
          property: secret
          version: "1"
        secretKey: secret

  registry-certificate-v1:
    refreshInterval: 0
    secretStoreName: gitlab-secrets
    target:
      creationPolicy: Owner
      deletionPolicy: Delete
    data:
      - remoteRef:
          key: "env/ops/ns/{{ .Release.Namespace }}/registry/certificate"
          property: registry-auth.crt
          version: "1"
        secretKey: registry-auth.crt
      - remoteRef:
          key: "env/ops/ns/{{ .Release.Namespace }}/registry/certificate"
          property: registry-auth.key
          version: "1"
        secretKey: registry-auth.key

  registry-httpsecret-v1:
    refreshInterval: 0
    secretStoreName: gitlab-secrets
    target:
      creationPolicy: Owner
      deletionPolicy: Delete
    data:
      - remoteRef:
          key: "env/ops/ns/{{ .Release.Namespace }}/registry/httpsecret"
          property: secret
          version: "1"
        secretKey: secret

  registry-postgres-credentials-v1:
    refreshInterval: 0
    secretStoreName: gitlab-secrets
    target:
      creationPolicy: Owner
      deletionPolicy: Delete
    data:
      - remoteRef:
          key: "env/ops/ns/{{ .Release.Namespace }}/registry/postgresql"
          property: password
          version: "1"
        secretKey: password

  registry-storage-v2:
    refreshInterval: 0
    secretStoreName: gitlab-secrets
    target:
      creationPolicy: Owner
      deletionPolicy: Delete
      template:
        data:
          config: |
            gcs:
              bucket: gitlab-ops-registry
              keyfile: /etc/docker/registry/storage/gcs.json
          gcs.json: |
            {{ `{{ "{{-" }}` }} .service_account_key -}}
    data:
      - remoteRef:
          key: "env/ops/ns/{{ .Release.Namespace }}/google"
          property: service_account_key
          version: "1"
        secretKey: service_account_key

  saml-v1:
    refreshInterval: 0
    secretStoreName: gitlab-secrets
    target:
      creationPolicy: Owner
      deletionPolicy: Delete
    data:
      - remoteRef:
          key: "env/ops/ns/{{ .Release.Namespace }}/auth/saml"
          property: provider
          version: "1"
        secretKey: provider

  shell-host-keys-v1:
    refreshInterval: 0
    secretStoreName: gitlab-secrets
    target:
      creationPolicy: Owner
      deletionPolicy: Delete
    data:
      {{- range $alg := tuple "dsa" "ecdsa" "ed25519" "rsa" }}
      - remoteRef:
          key: "env/ops/ns/{{ $.Release.Namespace }}/shell/host-keys"
          property: ssh_host_{{ $alg }}_key
          version: "1"
        secretKey: ssh_host_{{ $alg }}_key
      - remoteRef:
          key: "env/ops/ns/{{ $.Release.Namespace }}/shell/host-keys"
          property: ssh_host_{{ $alg }}_key.pub
          version: "1"
        secretKey: ssh_host_{{ $alg }}_key.pub
      {{- end }}

  shell-secret-v1:
    refreshInterval: 0
    secretStoreName: gitlab-secrets
    target:
      creationPolicy: Owner
      deletionPolicy: Delete
    data:
      - remoteRef:
          key: "env/ops/ns/{{ .Release.Namespace }}/shell/secret"
          property: secret
          version: "1"
        secretKey: secret

  smtp-password-v1:
    refreshInterval: 0
    secretStoreName: gitlab-secrets
    target:
      creationPolicy: Owner
      deletionPolicy: Delete
    data:
      - remoteRef:
          key: "env/ops/ns/{{ .Release.Namespace }}/smtp"
          property: password
          version: "1"
        secretKey: password

  storage-config-v2:
    refreshInterval: 0
    secretStoreName: gitlab-secrets
    target:
      creationPolicy: Owner
      deletionPolicy: Delete
    data:
      - remoteRef:
          key: "env/ops/ns/{{ .Release.Namespace }}/google"
          property: service_account_key
          version: "1"
        secretKey: service_account_key

  suggested-reviewers-secret-v1:
    refreshInterval: 0
    secretStoreName: gitlab-secrets
    target:
      creationPolicy: Owner
      deletionPolicy: Delete
    data:
      - remoteRef:
          key: "env/ops/ns/{{ .Release.Namespace }}/suggested-reviewers/secret"
          property: secret
          version: "1"
        secretKey: secret

  workhorse-secret-v1:
    refreshInterval: 0
    secretStoreName: gitlab-secrets
    target:
      creationPolicy: Owner
      deletionPolicy: Delete
    data:
      - remoteRef:
          key: "env/ops/ns/{{ .Release.Namespace }}/workhorse/secret"
          property: shared_secret
          version: "1"
        secretKey: shared_secret

secretStores:
  - name: gitlab-secrets
    role: gitlab

serviceAccount:
  name: gitlab-secrets
