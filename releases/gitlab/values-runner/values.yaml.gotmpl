# https://gitlab.com/gitlab-org/charts/gitlab-runner/-/blob/main/values.yaml
gitlabUrl: https://ops.gitlab.net

replicas: 2

checkInterval: 10
concurrent: 100

resources:
  requests:
    cpu: 500m
    memory: 2Gi
  limits:
    cpu: 1000m
    memory: 4Gi

rbac:
  create: true
  clusterWideAccess: false
  serviceAccountAnnotations:
    iam.gke.io/gcp-service-account: gitlab-ops-runner-sa@gitlab-ops.iam.gserviceaccount.com
  rules:
    - resources: ["configmaps", "pods", "pods/attach", "pods/exec", "secrets", "services"]
      verbs: ["get", "list", "watch", "create", "patch", "update", "delete"]
    - apiGroups: [""]
      resources: ["pods/exec"]
      verbs: ["create", "patch", "delete"]
    - apiGroups: [""]
      resources: ["events"]
      verbs: ["list"]

unregisterRunners: true

runners:
  secret: runner-secret-v2
  # https://docs.gitlab.com/runner/configuration/advanced-configuration.html
  # https://docs.gitlab.com/runner/executors/kubernetes.html
  config: |
    [[runners]]
      environment = [
        # Docker
        "DOCKER_HOST=tcp://docker:2376",
        "DOCKER_TLS_VERIFY=1",
        "DOCKER_TLS_CERTDIR=/docker/certs",
        "DOCKER_CERT_PATH=/docker/certs/client",
        "DOCKER_DRIVER=overlay2",
        # Needed for Docker-in-Docker on COS 105: https://gitlab.com/gitlab-com/gl-infra/production/-/issues/17303
        "DOCKER_IPTABLES_LEGACY=1",
        # Zip compression
        "CACHE_COMPRESSION_LEVEL=fast",
        "ARTIFACT_COMPRESSION_LEVEL=fast",
      ]
      # https://gitlab.com/gitlab-org/gitlab-runner/-/issues/27384#note_1198898488
      # TLS secured Docker-in-Docker containers may need some extra
      # time to generate key material before they are ready.  This
      # script implements the logic to wait if necessary.
      # See configMaps.check-docker-readiness for the implementation.
      pre_build_script = "sh /docker/scripts/check-readiness"

      [runners.feature_flags]
        # Fast cache ZIP
        FF_USE_FASTZIP = true

      [runners.kubernetes]
        image = "ruby:2.4-alpine"

        # For Docker-in-Docker
        privileged = true

        # Do not allow overrides from jobs
        namespace = "{{.Release.Namespace}}"
        namespace_overwrite_allowed = ""
        # Use a unpriviledged SA without token automount
        service_account = "runner-jobs"
        service_account_overwrite_allowed = ""
        pod_annotations_overwrite_allowed = ""
        bearer_token_overwrite_allowed = false

        # Pull policy
        allowed_pull_policies = ["if-not-present", "always"]
        pull_policy = ["if-not-present", "always"]

        # Pod resources
        cpu_request = "600m"
        cpu_request_overwrite_max_allowed = "4000m"
        memory_request = "1024Mi"
        memory_request_overwrite_max_allowed = "4096Mi"
        # Pod services
        service_cpu_request = "100m"
        service_memory_request = "256Mi"
        # Pod helper
        helper_cpu_request = "10m"
        helper_memory_request = "32Mi"

        # Allow jobs to terminate gracefully, 2h
        pod_termination_grace_period_seconds = 7200
        # Clean completed jobs immediately
        cleanup_grace_period_seconds = 30

        # Pod/job status polling
        poll_interval = 3
        poll_timeout = 300

      [runners.kubernetes.node_selector]
        "gitlab.com/runner" = "ops-central-k8s"
        "cloud.google.com/gke-ephemeral-storage-local-ssd" = "true"

      [runners.kubernetes.node_tolerations]
        "gitlab.com/runner=true" = "NoSchedule"

      [runners.kubernetes.pod_labels]
        "cluster-autoscaler.kubernetes.io/safe-to-evict" = "false"

      [[runners.kubernetes.volumes.empty_dir]]
        name = "docker-certs"
        mount_path = "/docker/certs/client"
        medium = "Memory"
        size_limit = "10Mi"

      [[runners.kubernetes.volumes.config_map]]
        # The name must match the runner's ConfigMap metadata.name.
        name = "{{ .Release.Name }}-gitlab-runner"
        mount_path = "/docker/scripts"
        [runners.kubernetes.volumes.config_map.items]
          "check-docker-readiness" = "check-readiness"

      [runners.cache]
        Type = "gcs"
        Path = "gitlab-runner"
        Shared = true
        [runners.cache.gcs]
          BucketName = "gitlab-ops-ci-cache"

configMaps:
  check-docker-readiness: |
    #!/bin/sh -eu

    # See https://gitlab.com/gitlab-org/gitlab-runner/-/issues/27384
    # for why this check is (still?) needed.

    # We get invoked in the build container.  This happens when all
    # services are ready, as far as GitLab Runner is concerned.  If a
    # docker service was requested from the CI/CD configuration, we
    # should be able to query the service for information when it's
    # ready.

    # Enable a trace in line with the GitLab Runner's trace support.

    test ${CI_DEBUG_TRUE+true} && set -x

    # If a docker service was requested, the build container can be
    # expected to have a docker command.  Absence of the command is
    # a good indication there is no need to wait for the service to
    # become ready.  Explicitly setting DOCKER_READINESS_TIMEOUT to
    # zero in your CI/CD configuration also means there is no need
    # to check.

    type docker >/dev/null 2>&1 || exit 0
    test ${DOCKER_READINESS_TIMEOUT:=30} -eq 0 && exit 0

    # The docker command may be available in the build container but
    # not used in the job.  In that case, requesting the service is
    # unnecessary and we should timeout checking after some time so
    # the job can proceed.  Change DOCKER_READINESS_TIMEOUT in your
    # CI/CD configuration if the default is not appropriate.

    slept=0
    until test $slept -eq $DOCKER_READINESS_TIMEOUT \
          || docker info >/dev/null 2>&1; do
        sleep 1
        slept=$((slept + 1))
    done

    if ! docker info >/dev/null 2>&1 \
       && test $slept -eq $DOCKER_READINESS_TIMEOUT; then
        # Point people to info on fixing their CI/CD configuration.
        # Use ANSI escape codes to colorize the message so it stands
        # out in the log.
        echo -e >&2 "\e[1m\e[93m"
        echo >&2 "Waited $DOCKER_READINESS_TIMEOUT seconds for the docker service to become ready."
        echo >&2 "This runner assumes a TLS-enabled Docker-in-Docker."
        echo >&2 "Did you update your CI/CD configuration already?"
        echo >&2 "https://gitlab.com/gitlab-org/gitlab-runner/-/issues/27384 for more information."
        echo -e >&2 "\e[0m"
    fi
