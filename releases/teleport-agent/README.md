# teleport-kube-agent

This release contains the cluster side components for Teleport.  These agent pods connect to the server compoments defined at https://gitlab.com/gitlab-com/gl-infra/teleport-server to authenticate users and validate roles.  These compoments are stateless and can be freely created and destroyed.  

Be mindful that forcing a delete on a pod may interrupt someone actively running a command or database query.  It's best to let the pods exit gracefully in environments where there are active sessions.

## Development

Run with the "minikube" Helmfile environment, e.g. `helmfile -e minikube apply`.
