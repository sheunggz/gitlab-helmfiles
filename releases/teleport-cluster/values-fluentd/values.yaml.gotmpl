# https://github.com/bitnami/charts/blob/main/bitnami/fluentd/values.yaml
---
tls:
  enabled: true
  aggregator:
    existingSecret: ""

metrics:
  enabled: true
  service:
    type: ClusterIP
    port: 24231
    # Annotations for the Prometheus Exporter service.
    # If port or path annotation is provided the values will be used to in the prometheus ServiceMonitor CRD.
    annotations:
      prometheus.io/scrape: "true"
      prometheus.io/port: "24231"
      prometheus.io/path: "/metrics"

# Forwarder is a DaemonSet for collecting container and cluster logs and sending them to Aggregator.
# We do NOT need the forwarder for the Teleport event-handler plugin.
forwarder:
  enabled: false

aggregator:
  enabled: true

  # This custom image is based on bitnami/fluentd image.
  # It has the fluent-plugin-gcloud-pubsub-custom installed.
  # See https://gitlab.com/gitlab-com/gl-infra/oci-images
  #
  image:
    registry: registry.gitlab.com
    repository: gitlab-com/gl-infra/oci-images/fluentd/bitnami-ext
    tag: "08cb2b51"

  replicaCount: 1

  serviceAccount:
    create: false
    name: ""

  resources:
    limits:
      cpu: 500m
      memory: 1Gi
    requests:
      cpu: 300m
      memory: 512Mi

  # Disable the port that the aggregator container will listen for logs since we do not use it.
  port: null

  containerPorts:
    - name: http
      containerPort: 9880
      protocol: TCP
    - name: teleport
      containerPort: 8888
      protocol: TCP

  service:
    type: ClusterIP
    ports:
      tcp: null
      http:
        port: 9880
        targetPort: http
        protocol: TCP
      teleport:
        port: 8888
        targetPort: teleport
        protocol: TCP

  # Name of the config file that will be used by Fluentd at launch under the `/opt/bitnami/fluentd/conf` directory
  configFile: fluentd.conf
  
  # Files to be added to be config map
  configMapFiles:
    fluentd.conf: |
      # Ignore fluentd own events
      <label @FLUENT_LOG>
        <match **>
          @type null
        </match>
      </label>

      @include metrics.conf
      @include fluentd-inputs.conf
      @include fluentd-output.conf
      @include fluentd-output-events.conf
      @include fluentd-output-sessions.conf
    metrics.conf: |
      # Prometheus Exporter Plugin

      # Input plugin that exports metrics
      <source>
        @type prometheus
        port 24231
      </source>

      # input plugin that collects metrics from MonitorAgent
      <source>
        @type prometheus_monitor
        <labels>
          host ${hostname}
        </labels>
      </source>

      # input plugin that collects metrics for output plugin
      <source>
        @type prometheus_output_monitor
        <labels>
          host ${hostname}
        </labels>
      </source>
    fluentd-inputs.conf: |
      # HTTP input for the liveness and readiness probes
      <source>
        @type http
        bind 0.0.0.0
        port 9880
      </source>

      # HTTP input for the teleport-event-handler
      <source>
        @type http
        port 8888

        <transport tls>
          client_cert_auth true
          ca_path /opt/bitnami/fluentd/certs/in_forward/ca.crt
          cert_path /opt/bitnami/fluentd/certs/in_forward/server.crt
          private_key_path /opt/bitnami/fluentd/certs/in_forward/server.key
        </transport>

        <parse>
          @type json
          json_parser oj

          # This time format is used by the plugin. This field is required.
          time_type string
          time_format %Y-%m-%dT%H:%M:%S
        </parse>
      </source>
    fluentd-output.conf: |
      # Throw the healthcheck to the standard output
      <match fluentd.healthcheck>
        @type stdout
      </match>
    fluentd-output-events.conf: |
      <match events.log>
        @type gcloud_pubsub
        project ""
        dest_project ""
        topic ""

        # If the number of events is high, fluentd will start failing the ingestion with the following error message:
        #   buffer space has too many data errors.
        #
        # The following configuration prevents data loss in case of a restart and overcomes the limitations of the default fluentd buffer configuration.
        # This configuration is optional.
        #
        # See https://docs.fluentd.org/configuration/buffer-section for more details.
        <buffer>
          @type file
          path /opt/bitnami/fluentd/logs/buffers/events
          chunk_limit_size 10M
          queue_limit_length 16
          flush_thread_count 8
          flush_interval 1s
          retry_max_interval 30
          retry_forever true
        </buffer>
      </match>
    fluentd-output-sessions.conf: |
      <match sessions.log>
        @type stdout

        # If the number of events is high, fluentd will start failing the ingestion with the following error message:
        #   buffer space has too many data errors.
        #
        # The following configuration prevents data loss in case of a restart and overcomes the limitations of the default fluentd buffer configuration.
        # This configuration is optional.
        #
        # See https://docs.fluentd.org/configuration/buffer-section for more details.
        <buffer>
          @type file
          path /opt/bitnami/fluentd/logs/buffers/sessions
          chunk_limit_size 10M
          queue_limit_length 16
          flush_thread_count 8
          flush_interval 1s
          retry_max_interval 30
          retry_forever true
        </buffer>
      </match>
